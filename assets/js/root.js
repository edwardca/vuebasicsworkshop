var app = new Vue({
    el: '#listApp',
    data() {
        return {
            objetos: [{ nombre: 'objeto1', precio: 200 }, { nombre: 'objeto2', precio: 500 }, { nombre: 'objeto3', precio: 300 }],
            objetosEnCarrito: [],
            total: 0
        }
    },
    computed: {
        precioTotal() {
            total = 0;
            if (this.objetosEnCarrito.length > 0) {
                objetosEnCarrito.forEach(element => {
                    total += element.precio;
                });
            }
            return total;
        }
    }
})